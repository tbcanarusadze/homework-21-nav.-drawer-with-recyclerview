package com.example.homework21

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_menu_recyclerview_layout.view.*
import kotlinx.android.synthetic.main.nav_header_main.view.*

class MenuRecyclerViewAdapter(
    private val items: MutableList<MenuModel>,
    private val activity: MainActivity
) :
    RecyclerView.Adapter<MenuRecyclerViewAdapter.ViewHolder>() {

    private var currentPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_menu_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size

    private lateinit var model: MenuModel

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind() {
            model = items[adapterPosition]
            itemView.iconImageView.setImageResource(model.icon)
            itemView.title.text = model.title

            if (currentPosition == adapterPosition) {
                itemView.setBackgroundColor(
                    ContextCompat.getColor(
                        activity,
                        android.R.color.holo_green_light
                    )
                )
            } else {
                itemView.setBackgroundColor(ContextCompat.getColor(activity, android.R.color.white))
            }
            itemView.setOnClickListener {
                currentPosition = adapterPosition
                notifyDataSetChanged()

            }
        }



    }
}


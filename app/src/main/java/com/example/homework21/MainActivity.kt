package com.example.homework21

import android.os.Bundle
import android.util.Log.d
import android.view.Menu
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.navigation.NavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework21.ui.gallery.GalleryFragment
import com.example.homework21.ui.home.HomeFragment
import com.example.homework21.ui.slideshow.SlideshowFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_layout.*

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var menuRecyclerViewAdapter: MenuRecyclerViewAdapter
    private val items = mutableListOf<MenuModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initMenuItems()
        init()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    private fun init(){
        setSupportActionBar(toolbar)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow), drawer_layout)
        appBarConfiguration = AppBarConfiguration(navController.graph, drawer_layout)

        setupActionBarWithNavController(navController, appBarConfiguration)
        nav_view.setupWithNavController(navController)

    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(drawer_layout) || super.onSupportNavigateUp()
    }

    private fun initMenuItems(){
        menuRecyclerViewAdapter = MenuRecyclerViewAdapter(items, this)
        menuRecyclerView.layoutManager = LinearLayoutManager(this)
        menuRecyclerView.adapter = menuRecyclerViewAdapter

        items.add(MenuModel(R.drawable.ic_menu_camera, "camera"))
        items.add(MenuModel(R.drawable.ic_menu_gallery, "gallery"))
        items.add(MenuModel(R.drawable.ic_menu_slideshow, "slideshow"))
        items.add(MenuModel(R.drawable.ic_menu_send, "send"))
        items.add(MenuModel(R.drawable.ic_menu_share, "share"))
        items.add(MenuModel(R.drawable.ic_menu_manage, "manage"))



    }

//    private fun addFragment(fragment: Fragment, tag: String) {
//        val transaction = supportFragmentManager.beginTransaction()
//        transaction.replace(R.id.nav_host_fragment, fragment, tag)
//        transaction.addToBackStack(tag)
//        transaction.commit()
//    }

}
